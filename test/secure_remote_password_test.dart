import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';

import 'package:secure_remote_password/secure_remote_password.dart';
import 'package:secure_remote_password/src/types.dart';

void main() {
  group('SRPLogin', () {
    final srp = SRPLogin(username: "john.doe@exampl.com", password: "123456");

    test('startAuthentication', () {
      expect(base64.decode(srp.startAuthentication()), equals(srp.ephemeral.public));
    });

    // TODO: complete tests
    //test('processChallenge', () {
    //  srp.processChallenge(
    //    salt: salt,
    //    serverChallenge: serverChallenge,
    //  );
    //});
  });
}
