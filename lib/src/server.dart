import 'dart:convert';
import 'dart:math';

import 'package:flutter/foundation.dart';

import './bigint_ex.dart';
import './params.dart';
import './types.dart';

class Server {
  static Ephemeral generateEphemeral(String verifier) {
    final N = Params.N;
    final g = Params.g;
    final k = Params.k;

    final v = BigInt.parse(verifier, radix: 16);

    final random = Random.secure();
    final randomBytes = List<int>.generate(Params.hashOutputBytes, (i) => random.nextInt(256));

    final b = randomBytes.toBigInt();
    final B = (k.toBigInt() * v + g.modPow(b, N)) % N;

    return Ephemeral(secret: randomBytes, public: B.toBytes());
  }

  static Future<Session> deriveSession(
      String serverSecretEphemeral,
      String clientPublicEphemeral, String salt, String username,
      String verifier, String clientSessionProof) async {
    final res = await compute(_deriveSession,
        <String, String>{
          'serverSecretEphemeral': serverSecretEphemeral,
          'clientPublicEphemeral': clientPublicEphemeral,
          'salt': salt,
          'username': username,
          'verifier': verifier,
          'clientSessionProof': clientSessionProof,
        }
    );
    return Session(key: base64.decode(res['key']!), proof: base64.decode(res['proof']!));
  }

  static Map<String, String> _deriveSession(Map<String, String> params) {
    final serverSecretEphemeral = params['serverSecretEphemeral']!;
    final clientPublicEphemeral = params['clientPublicEphemeral']!;
    final salt = params['salt']!;
    final username = params['username']!;
    final verifier = params['verifier']!;
    final clientSessionProof = params['clientSessionProof']!;
    
    final N = Params.N;
    final g = Params.g;
    final k = Params.k;
    final H = Params.H;

    final b = BigInt.parse(serverSecretEphemeral, radix: 16);
    final A = BigInt.parse(clientPublicEphemeral, radix: 16);
    final s = BigInt.parse(salt, radix: 16);
    final I = username;
    final v = BigInt.parse(verifier, radix: 16);

    final B = (k.toBigInt() * v + g.modPow(b, N)) % N;

    if (A % N == 0) {
      throw Exception('the client sent an invalid public ephemeral');
    }

    final u = H([A, B]);

    final S = (A * (v.modPow(u.toBigInt(), N))).modPow(b, N);

    final K = H([S]);

    final M = H([
      H([N]).toBigInt() ^ H([g]).toBigInt(),
      H([I]),
      s,
      A,
      B,
      K
    ]);

    final expected = M;
    final actual = BigInt.parse(clientSessionProof, radix: 16);

    if (actual != expected) {
      throw Exception('Client provided session proof is invalid');
    }

    final P = H([A, M, K]);

    return {
      'key': base64.encode(K),
      'proof': base64.encode(P),
    };
  }
}


