
import 'dart:convert';

import 'package:secure_remote_password/src/client.dart';
import 'package:secure_remote_password/src/types.dart';


class SRPLogin {
  final String username;
  final String password;

  final Ephemeral ephemeral;
  Session? _session;

  SRPLogin({
    required this.username,
    required this.password,
  }): ephemeral = Client.generateEphemeral();

  /// Returns the base64 encoded client challenge
  String startAuthentication() {
    return base64.encode(ephemeral.public);
  }

  /// [salt] and [serverChallenge] are base64 encoded, returns base64 encoded `m`
  Future<String> processChallenge({required String salt, required String serverChallenge}) async {
    final decodedSalt = base64.decode(salt);
    _session = Client.deriveSession(
      ephemeral.secret,
      base64.decode(serverChallenge),
      decodedSalt,
      username,
      Client.derivePrivateKey(
        decodedSalt,
        username,
        password,
      ),
    );
    return base64.encode(_session!.proof);
  }

  /// [hamk] is base64 encoded
  Future<bool> verifySession({required String hamk}) async {
    return Client.verifySession(
      ephemeral.public,
      _session!,
      base64.decode(hamk),
    );
  }
}


class SRPSignUp {
  final String username;
  final String password;
  final List<int> _salt;
  List<int>? _cachedPrivateKey;
  List<int>? _cachedVerifier;

  SRPSignUp({required this.username, required this.password})
      : _salt = Client.generateSalt();

  String get privateKey {
    _cachedPrivateKey ??= Client.derivePrivateKey(_salt, username, password);
    return base64.encode(_cachedPrivateKey!);
  }
  
  String get verifier {
    final _ = privateKey;
    _cachedVerifier ??= Client.deriveVerifier(_cachedPrivateKey!);
    return base64.encode(_cachedVerifier!);
  }

  String get salt {
    return base64.encode(_salt);
  }
}
