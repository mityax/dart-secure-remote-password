class Session {
  List<int> key;
  List<int> proof;

  Session({required this.key, required this.proof});
}

class Ephemeral {
  List<int> public;
  List<int> secret;

  Ephemeral({required this.public, required  this.secret});
}
