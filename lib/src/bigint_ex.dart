
import 'dart:typed_data';

extension BigIntEx on BigInt {
  /*String toHex({bool evenLen = true}) {
    var hexStr = toRadixString(16);
    if (evenLen && !hexStr.length.isEven) {
      hexStr = '0$hexStr';
    }
    return hexStr;
  }*/

  /// Encode a BigInt into bytes using big-endian encoding.
  Uint8List toBytes() {
    return _encodeBigInt(this);
  }
}


extension ByteListExt on List<int> {
  BigInt toBigInt() {
    return bytesToBigInt(this);
  }
}


// The following methods are taken from Pointy Castle.
// See https://github.com/PointyCastle/pointycastle/blob/master/lib/src/utils.dart

/// Decode a BigInt from bytes in big-endian encoding.
BigInt bytesToBigInt(List<int> bytes) {
  BigInt result = BigInt.from(0);
  for (int i = 0; i < bytes.length; i++) {
    result += BigInt.from(bytes[bytes.length - i - 1]) << (8 * i);
  }
  return result;
}

final _byteMask = BigInt.from(0xff);

/// Encode a BigInt into bytes using big-endian encoding.
Uint8List _encodeBigInt(BigInt number) {
  // Not handling negative numbers.
  int size = (number.bitLength + 7) >> 3;
  final result = Uint8List(size);
  for (int i = 0; i < size; i++) {
    result[size - i - 1] = (number & _byteMask).toInt();
    number = number >> 8;
  }
  return result;
}
