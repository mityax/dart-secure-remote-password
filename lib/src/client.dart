import 'dart:math';

import 'package:flutter/foundation.dart';

import './bigint_ex.dart';
import './params.dart';
import './types.dart';


class Client {
  static List<int> generateSalt() {
    final random = Random.secure();
    return List<int>.generate(Params.hashOutputBytes, (i) => random.nextInt(256));
  }

  static List<int> derivePrivateKey(List<int> salt, String username, String password) {
    const H = Params.H;
    final s = salt.toBigInt();
    final x = H([
      s,
      H(['$username:$password'])
    ]);

    return x;
  }

  static List<int> deriveVerifier(List<int> privateKey) {
    final N = Params.N;
    final g = Params.g;
    return g.modPow(privateKey.toBigInt(), N).toBytes();
  }

  static Ephemeral generateEphemeral() {
    final N = Params.N;
    final g = Params.g;
    final random = Random.secure();
    final randomBytes = List<int>.generate(Params.hashOutputBytes, (i) => random.nextInt(256));

    final a = randomBytes.toBigInt();
    final A = g.modPow(a, N);

    return Ephemeral(secret: randomBytes, public: A.toBytes());
  }

  static Session deriveSession(List<int> clientSecretEphemeral,
      List<int> serverPublicEphemeral, List<int> salt, String username,
      List<int> privateKey) {
    final N = Params.N;
    final g = Params.g;
    final k = Params.k;
    const H = Params.H;

    final a = clientSecretEphemeral.toBigInt();
    final B = serverPublicEphemeral.toBigInt();
    final s = salt.toBigInt();
    final I = username;
    final x = privateKey.toBigInt();

    final A = g.modPow(a, N);

    if (B % N == BigInt.from(0)) {
      throw Exception('The server sent an invalid public ephemeral');
    }

    final u = H([A, B]);

    final S = (B - (k.toBigInt() * (g.modPow(x, N)))).modPow(a + (u.toBigInt() * x), N);

    final K = H([S]);

    final M = H([
      H([N]).toBigInt() ^ H([g]).toBigInt(),
      H([I]),
      s,
      A,
      B,
      K
    ]);

    return Session(key: K, proof: M);
  }

  static bool verifySession(List<int> clientPublicEphemeral, Session clientSession,
      List<int> serverSessionProof) {
    const H = Params.H;
    final A = clientPublicEphemeral.toBigInt();
    final M = clientSession.proof.toBigInt();
    final K = clientSession.key.toBigInt();

    final expected = H([A, M, K]);
    final actual = serverSessionProof;

    if (!listEquals(actual, expected)) {
      return false;
    }
    return true;
  }
}
